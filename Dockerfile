FROM maven:latest AS build
RUN apt-get update \
  && apt-get install -y curl procps \
  && rm -rf /var/lib/apt/lists/*
WORKDIR /
COPY src /src
COPY pom.xml /
RUN mvn clean package


FROM openjdk:alpine
COPY --from=build /target/capstoneproj-1.0-SNAPSHOT.jar /capstoneproj-1.0-SNAPSHOT.jar
CMD java -cp /capstoneproj-1.0-SNAPSHOT.jar com.ken.App

